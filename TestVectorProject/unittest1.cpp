#include "stdafx.h"
#include "CppUnitTest.h"
#include "MyVector.h" 
#include <functional>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
class A {};
namespace Microsoft {
	namespace VisualStudio {
		namespace CppUnitTestFramework {
			template<typename T>
			std::wstring ToString(const MyVector<T> & mv) {
				std::wstringstream ws;
				for (T const & t : mv) {
					ws << ToString(t) << L" ";
				}
				return ws.str();
			}
		}
	}
}
//todo: provide a ToString method for iterators.

//namespace Microsoft { --> not working
//	namespace VisualStudio {
//		namespace CppUnitTestFramework {
//			template<typename T>
//			std::wstring ToString(const MyVector<T>::iterator & it) {
//				std::wstringstream ws;
//				ws << ToString(it.getVecPtr()) << L": " << ToString(it.getPos());
//				return ws.str();
//			}
//		}
//	}
//}
namespace TestVectorProject
{
	TEST_CLASS(UnitTest1)
	{
	private:
		MyVector<int> start1;
	public:
		TEST_METHOD_INITIALIZE(init) {
			start1.push_back(3);
			start1.push_back(4);
			start1.push_back(4525435);
			start1.push_back(-4324);
			start1.push_back(32);
			start1.push_back(64);
			start1.push_back(128);
			start1.push_back(256);
		}
		TEST_METHOD(eraseSingle1)
		{
			MyVector<int> v{};
			v.push_back(3);
			v.push_back(4);
			v.push_back(4525435);
			v.push_back(32);
			v.push_back(64);
			v.push_back(128);
			v.push_back(256);

			start1.erase(start1.begin() + 3);
			Assert::AreEqual(start1, v);
		}
		TEST_METHOD(eraseSingle2)
		{
			MyVector<int> v{};
			v.push_back(4);
			v.push_back(4525435);
			v.push_back(-4324);
			v.push_back(32);
			v.push_back(64);
			v.push_back(128);
			v.push_back(256);

			start1.erase(start1.begin());
			Assert::AreEqual(start1, v);
		}
		TEST_METHOD(eraseBeforeEnd) {
			MyVector<int> v{};
			v.push_back(3);
			v.push_back(4);
			v.push_back(4525435);
			v.push_back(-4324);
			v.push_back(32);
			v.push_back(64);
			v.push_back(128);
			start1.erase(start1.end() - 1);
			Assert::AreEqual(v, start1);
		}
		TEST_METHOD(eraseBeforeEndGoodResIterator) {
			MyVector<int>::iterator it = start1.erase(start1.end() - 1);
			Assert::IsTrue(it == start1.end());
		}
		TEST_METHOD(eraseSingleBadIterator)
		{
			std::function<MyVector<int>::iterator(void)> f1 = [this] {return start1.erase(start1.end()); };
			Assert::ExpectException<IllegalArgumentException>(f1);
		}
		TEST_METHOD(eraseRange1)
		{
			MyVector<int> v{};
			v.push_back(32);
			v.push_back(64);
			v.push_back(128);
			v.push_back(256);

			auto it = start1.erase(start1.begin(), start1.begin() + 4);
			Assert::AreEqual(start1, v);
			Assert::IsTrue(it == start1.begin());
			Assert::IsTrue(start1.size() == 4);
		}
		TEST_METHOD(eraseRange2)
		{
			MyVector<int> v{};
			v.push_back(3);
			v.push_back(4);
			v.push_back(4525435);
			v.push_back(-4324);
			auto it = start1.erase(start1.begin() + 4, start1.end());
			Assert::AreEqual(start1, v);
			Assert::IsTrue(it == start1.end() - 1);
			Assert::IsTrue(start1.size() == 4);
		}
		TEST_METHOD(eraseRange3)
		{
			MyVector<int> v{};
			v.push_back(3);
			v.push_back(4);
			v.push_back(4525435);
			v.push_back(-4324);
			v.push_back(128);
			v.push_back(256);
			auto it = start1.erase(start1.begin() + 4, start1.end() - 2);
			Assert::AreEqual(start1, v);
			Assert::IsTrue(it == start1.end() - 3);
			Assert::IsTrue(start1.size() == 6);
			Assert::IsTrue(*it == -4324);
		}
		TEST_METHOD(eraseRange4)
		{
			MyVector<int> v{};
			auto it = start1.erase(start1.begin(), start1.end());
			Assert::AreEqual(start1, v);
			Assert::IsTrue(it == start1.end());
			Assert::IsTrue(it == start1.begin());
			Assert::IsTrue(start1.size() == 0);
		}
		TEST_METHOD(eraseRangeFail)
		{
			std::function<MyVector<int>::iterator(void)> f1 = [this] {return start1.erase(start1.begin() + 2, start1.begin() + 1); };
			Assert::ExpectException<IllegalArgumentException>(f1);
		}
		TEST_METHOD(insertGoodItValue) {
			MyVector<int> v{};
			v.push_back(3);
			v.push_back(4);
			v.push_back(4525435);
			v.push_back(-4324);
			v.push_back(32);
			v.push_back(64);
			v.push_back(-1337);
			v.push_back(128);
			v.push_back(256);
			auto resit = start1.insert(start1.end() - 2, -1337);
			Assert::AreEqual(*resit, -1337);
		}
		TEST_METHOD(insert1) {
			MyVector<int> v{};
			v.push_back(3);
			v.push_back(4);
			v.push_back(4525435);
			v.push_back(-4324);
			v.push_back(32);
			v.push_back(64);
			v.push_back(-1337);
			v.push_back(128);
			v.push_back(256);
			auto resit = start1.insert(start1.end() - 2, -1337);
			Assert::AreEqual(v, start1);
		}
		TEST_METHOD(insert2) {
			MyVector<int> v{};
			v.push_back(-1337);
			v.push_back(3);
			v.push_back(4);
			v.push_back(4525435);
			v.push_back(-4324);
			v.push_back(32);
			v.push_back(64);
			v.push_back(128);
			v.push_back(256);
			auto resit = start1.insert(start1.begin(), -1337);
			Assert::AreEqual(v, start1);
		}
		TEST_METHOD(insert3) {
			MyVector<int> v{};
			v.push_back(3);
			v.push_back(4);
			v.push_back(4525435);
			v.push_back(-4324);
			v.push_back(32);
			v.push_back(64);
			v.push_back(128);
			v.push_back(256);
			v.push_back(1337);
			int x = -1337;
			auto resit = start1.insert(start1.end(), -x);
			Assert::AreEqual(v, start1);
		}
		TEST_METHOD(iteratorDiffVecNotEqualDebug) { 
			MyVector<int> v1{};
			MyVector<int> v2{};
			v1.push_back(1);
			v1.push_back(1);
			std::function<bool(void)> f1 = [v1, v2] {return (v1.begin() == v2.begin()); };
			Assert::ExpectException<IteratorMismatchException>(f1);
		}
		//TEST_METHOD(iteratorSameVecDiff) {
		//	MyVector<int> v{};
		//	v.push_back(32);
		//	v.push_back(0);
		//	v.push_back(-1);
		//	Assert::IsTrue(v.begin() == v.begin() + 1);
		//	Assert::IsTrue(v.end() == v.begin());
		//	Assert::IsTrue(v.end() - 1 == v.begin());
		//}
		TEST_METHOD(plusEqual) {
			auto it1 = start1.begin() + 2;
			auto & it2 = it1;
			Assert::IsTrue(it1 == it2);
			Assert::AreEqual(*it1, 4525435);
			Assert::AreEqual(*it2, 4525435);
			it1 += 1;
			Assert::AreEqual(*it1, -4324);
			Assert::AreEqual(*it2, -4324);
			it2 += 4;
			Assert::AreEqual(*it1, 256);
			Assert::AreEqual(*it2, 256);
			it1 += 1;
			Assert::IsTrue(it1 == start1.end());
			Assert::IsTrue(it2 == start1.end());

		}
		TEST_METHOD(minusEqual) {
			auto it1 = start1.end();
			auto & it2 = it1;
			Assert::IsTrue(it1 == it2);
			it1 -= 1;
			it2 -= 1;
			Assert::IsTrue(it1 == it2);
			Assert::AreEqual(*it2, 128);

			it1 -= 3;
			Assert::AreEqual(*it1, -4324);
			it2 -= 5739457;
			Assert::IsTrue(it2 == start1.begin());
		}
		TEST_METHOD(plusplusprefix) {
			auto it1 = start1.begin() + 1; //4
			Assert::IsTrue(++it1 == start1.begin() + 2);
			Assert::AreEqual(*(++it1), -4324);
		}
		TEST_METHOD(plusplusPostFix) {
			auto it1 = start1.begin() + 4;
			Assert::AreEqual(*(it1++), 32);
			Assert::AreEqual(*it1, 64);
		}
		TEST_METHOD(minusminusPrefix) {
			auto it = start1.end();
			Assert::AreEqual(*(--it), 256);
			Assert::AreEqual(*(--it), 128);
		}
		TEST_METHOD(minusminusPostfix) {
			auto it = start1.begin() + 4;
			Assert::AreEqual(*(it--), 32);
			Assert::AreEqual(*it, -4324);
		}
		TEST_METHOD(substractTwoIterators) {
			MyVector<int> v{};
			Assert::AreEqual(v.end() - start1.begin(), 0);
			Assert::AreEqual(start1.begin() - start1.end(), -8);
			Assert::AreEqual((start1.end() - 1) - (start1.begin() + 2), 5);
		}
		TEST_METHOD(lessThan) {
			Assert::IsTrue(start1.begin() < start1.end());
			Assert::IsTrue(start1.begin() + 1 < start1.end() - 2);
			Assert::IsFalse(start1.end() < start1.end() - 4);
		}
		TEST_METHOD(lessOrEqual) {
			Assert::IsTrue(start1.begin() < start1.end());
			Assert::IsTrue(start1.begin() + 1 < start1.end() - 2);
			Assert::IsFalse(start1.end() < start1.end() - 4);
			Assert::IsTrue(start1.begin() <= start1.begin());
			Assert::IsTrue(start1.end() <= start1.end());
			Assert::IsTrue(start1.begin() + 5 <= start1.begin() + 5);
		}

	};
}