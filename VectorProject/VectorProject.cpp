// VectorProject.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MyVector.h"

int main()
{
	using std::cout;
	using std::endl;
	using std::vector;

	MyVector<int> v{};
	const MyVector<double> v2{};
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	//MyVector<double>::const_iterator fit = v2.end();
	auto it = v.begin() + 1;
	cout << *it << endl;
	v.erase(v.begin());
	cout << *it << endl;
    return 0;
}

