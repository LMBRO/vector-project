#ifndef VECTOR_H
#define VECTOR_H

#include <algorithm>
#include <iostream>
#include <stdexcept>
#include "dsexceptions.h"
//template <typename Object>
//class IteratorManager
//{
//public:
//private:
//	std::multimap<MyVector<Object>::iterator, MyVector<Object>::iterator*>;
//
//};
template <typename Object>
class MyVector
{
private:
	int theSize;
	int theCapacity;
	Object * objects;
public:
	class const_iterator {
	private:
		unsigned int thePos;
		MyVector<Object> const *  theVec;
	public:
		const_iterator() : thePos{ 0 }, theVec{ nullptr } {}
		const_iterator(unsigned int pos, MyVector<Object> const * const  pvec) :thePos{ pos }, theVec{ pvec } {}
		~const_iterator() {}
		const_iterator(const_iterator const & rhs) :thePos{ rhs.thePos }, theVec{ rhs.theVec } {}
		const_iterator(const_iterator && rhs) :thePos{ std::move(rhs.thePos) }, theVec{ std::move(rhs.theVec) } {}
		const_iterator & operator=(const const_iterator & rhs) {
			if (this != &rhs) {
				thePos = rhs.thePos;
				theVec = rhs.theVec;
			}
			return *this;
		}
		const_iterator & operator=(const_iterator && rhs){
			return *this = rhs;
		}

		unsigned int getPos() const {
			return thePos;
		}
		MyVector<Object> * getVecPtr() const {
			return theVec;
		}
		const Object & operator* () const {
			return (*theVec)[thePos];
		}
		const_iterator & operator+=(int n) {
			int res = thePos + n;
			if (res < 0)
				thePos = 0;
			else if (res > theVec->size())
				thePos = theVec->size();
			else
				thePos = res;
			return *this;
		}
		const_iterator & operator-=(int n) {
			return *this += -n;
		}
		const_iterator const operator+(int n) const {
			return const_iterator(*this) += n;
		}
		const_iterator const operator-(int n) const {
			return const_iterator(*this) -= n;
		}
		const_iterator & operator++() {
			return *this += 1;
		}
		const_iterator operator++(int) {
			const_iterator old = *this;
			++(*this);
			return old;
		}
		const_iterator & operator--() {
			return *this -= 1;
		}
		const_iterator operator--(int) {
			const_iterator old = *this;
			--(*this);
			return old;
		}
		int operator-(const_iterator const & rhs) const {
			return rhs.thePos - thePos;
		}
		bool operator==(const_iterator const & rhs) const {
			if (theVec != rhs.theVec)
				throw IteratorMismatchException{}; //eventually (never) false should be returned when in release debug
												  // by checking the debug macro defined by the compiler
			return thePos == rhs.thePos;
		}
		bool operator!=(const_iterator const & rhs) const {
			if (theVec != rhs.theVec)
				throw IteratorMismatchException{};
			return !(*this == rhs);
		}
		bool operator<(const_iterator const & rhs) const {
			if (theVec != rhs.theVec)
				throw IteratorMismatchException{};
			return thePos < rhs.thePos;
		}
		bool operator<=(const_iterator const & rhs) const {
			if (theVec != rhs.theVec)
				throw IteratorMismatchException{};
			return thePos <= rhs.thePos;
		}
		bool operator>(const_iterator const & rhs) const {
			if (theVec != rhs.theVec)
				throw IteratorMismatchException{};
			return thePos > rhs.thePos;
		}
		bool operator>=(const_iterator const & rhs) const {
			if (theVec != rhs.theVec)
				throw IteratorMismatchException{};
			return thePos >= rhs.thePos;
		}
	};
	class iterator {
	private:
		unsigned int thePos;
		MyVector<Object> *  theVec; //iterator doesn't have ownership of this resource!
	public:
		iterator() : thePos{ 0 }, theVec{ nullptr } {}
		iterator(unsigned int pos, MyVector<Object> * const  pvec) :thePos{ pos }, theVec{ pvec } { }
		iterator(iterator const & rhs) :thePos{ rhs.thePos }, theVec{ rhs.theVec } {}
		iterator(iterator && rhs) : thePos{ std::move(rhs.thePos) }, theVec{ std::move(rhs.theVec) } {}
		iterator & operator=(iterator && rhs) {
			 return *this = rhs;
		}
		iterator & operator=(const iterator & rhs) {
			if (this != &rhs) {
				thePos = rhs.thePos;
				theVec = rhs.theVec;
			}
			return *this;
		}
		Object & operator*() const {
			return (*theVec)[thePos];
		}
		iterator & operator+=(int n) {
			int res = thePos + n;
			if (res < 0)
				thePos = 0;
			else if (res > theVec->size())
				thePos = theVec->size();
			else
				thePos = res;
			return *this;
		}
		iterator & operator-=(int n) {
			return *this += -n;
		}
		iterator const operator+(int n) const {
			return iterator(*this) += n;
		}
		iterator const operator-(int n) const {
			return iterator(*this) -= n;
		}
		iterator & operator++() {
			return *this += 1;
		}
		iterator operator++(int) {
			iterator old = *this;
			++(*this);
			return old;
		}
		iterator & operator--() {
			return *this -= 1;
		}
		iterator operator--(int) {
			iterator old = *this;
			--(*this);
			return old;
		}
		int operator-(iterator const & rhs) const {
			return thePos - rhs.thePos ;
		}
		bool operator==(iterator const & rhs) const {
			if (theVec != rhs.theVec)
				throw IteratorMismatchException{};
			return thePos == rhs.thePos;
		}
		bool operator!=(iterator const & rhs) const {
			if (theVec != rhs.theVec)
				throw IteratorMismatchException{};
			return !(*this == rhs);
		}
		bool operator<(iterator const & rhs) const {
			if (theVec != rhs.theVec)
				throw IteratorMismatchException{};
			return thePos < rhs.thePos;
		}
		bool operator<=(iterator const & rhs) const {
			if (theVec != rhs.theVec)
				throw IteratorMismatchException{};
			return thePos <= rhs.thePos;
		}
		bool operator>(iterator const & rhs) const {
			if (theVec != rhs.theVec)
				throw IteratorMismatchException{};
			return thePos > rhs.thePos;
		}
		bool operator>=(iterator const & rhs) const {
			if (theVec != rhs.theVec)
				throw IteratorMismatchException{};
			return thePos >= rhs.thePos;
		}

	};
	explicit MyVector(int initSize = 0)
		: theSize{ initSize }, theCapacity{ initSize + SPARE_CAPACITY }
	{
		objects = new Object[theCapacity];
	}

	MyVector(const MyVector & rhs)
		: theSize{ rhs.theSize }, theCapacity{ rhs.theCapacity }, objects{ nullptr }
	{
		objects = new Object[theCapacity];
		for (int k = 0; k < theSize; ++k)
			objects[k] = rhs.objects[k];
	}

	MyVector & operator= (const MyVector & rhs)
	{
		MyVector copy = rhs;
		std::swap(*this, copy);
		return *this;
	}

	~MyVector()
	{
		delete[] objects;
	}

	MyVector(MyVector && rhs)
		: theSize{ rhs.theSize }, theCapacity{ rhs.theCapacity }, objects{ rhs.objects }
	{
		rhs.objects = nullptr;
		rhs.theSize = 0;
		rhs.theCapacity = 0;
	}

	MyVector & operator= (MyVector && rhs)
	{
		std::swap(theSize, rhs.theSize);
		std::swap(theCapacity, rhs.theCapacity);
		std::swap(objects, rhs.objects);

		return *this;
	}

	bool empty() const
	{
		return size() == 0;
	}
	unsigned int size() const
	{
		return theSize;
	}
	unsigned int capacity() const
	{
		return theCapacity;
	}

	Object & operator[](int index)
	{
#ifndef NO_CHECK
		if (index < 0 || index >= size())
			throw ArrayIndexOutOfBoundsException{ };
#endif
		return objects[index];
	}

	const Object & operator[](int index) const
	{
#ifndef NO_CHECK
		if (index < 0 || index >= size())
			throw ArrayIndexOutOfBoundsException{ };
#endif
		return objects[index];
	}

	void resize(int newSize)
	{
		if (newSize > theCapacity)
			reserve(newSize * 2);
		theSize = newSize;
	}

	void reserve(int newCapacity)
	{
		if (newCapacity < theSize || newCapacity <= theCapacity)
			return;

		Object *newArray = new Object[newCapacity];
		for (int k = 0; k < theSize; ++k)
			newArray[k] = std::move(objects[k]);

		theCapacity = newCapacity;
		std::swap(objects, newArray);
		delete[] newArray;
	}


	void push_back(const Object & x)
	{
		if (theSize == theCapacity)
			reserve(2 * theCapacity + 1);
		objects[theSize++] = x;
	}

	void push_back(Object && x)
	{
		if (theSize == theCapacity)
			reserve(2 * theCapacity + 1);
		objects[theSize++] = std::move(x);
	}

	void pop_back()
	{
		if (empty())
			throw UnderflowException{ };
		--theSize;
	}

	const Object & back() const
	{
		if (empty())
			throw UnderflowException{ };
		return objects[theSize - 1];
	}
	// Iterator stuff: not bounds checked
	//typedef Object * iterator;
	//typedef const Object * const_iterator;

	iterator erase(iterator pitr)
	{
		Object * new_objects = new Object[theCapacity];
		bool erased = false;
		unsigned int i = 0;
		iterator retit;
		bool retit_set = false;
		for (iterator itr = begin(); itr != end(); ++itr) {

			if (itr == pitr) {
				erased = true;
				if (itr + 1 == end()) {
					retit = iterator(i, this);
				}
			}
			else {
				new_objects[i] = std::move(*itr);
				if (!retit_set && erased == true) {
					retit = iterator(i, this);
					retit_set = true;
				}
				++i;
			}
		}
		if (!erased)
			throw IllegalArgumentException{};
		//Modify the vector after checking if anything was erased to provide exception safety
		--theSize;
		std::swap(objects, new_objects);
		delete[] new_objects;
		return retit;

	}
	//erase begin and stop at end
	iterator erase(iterator sitr, iterator eitr)
	{
		Object * new_objects = new Object[theCapacity];
		iterator retit;
		unsigned int i = 0;
		unsigned int erase_count = 0;
		bool retit_set = false;
		bool sitr_met = false;
		bool eitr_met = false;
		bool eitr_is_end = false;
		bool sitr_is_begin = false;
		if (sitr == eitr)
			//This illegal condition would be caught anyway but  we can detect it easily so why not
			throw IllegalArgumentException{};
		if (eitr == end())
			eitr_is_end = true; // this means that eitr_is_met will never become true. This means we
								// will to set retit ourselfves.
		if (sitr == begin())
			sitr_is_begin = true;

		for (iterator itr = begin(); itr != end(); ++itr) {
			if (itr == sitr) {
				sitr_met = true;
				++erase_count;
			}
			else if (sitr_met && !eitr_met && itr != eitr)
				++erase_count;
			else if (!sitr_met && itr == eitr) {
				throw IllegalArgumentException{};
			}
			else if (!sitr_met || sitr_met && itr == eitr || sitr_met && eitr_met) {
				new_objects[i] = std::move(*itr);
				if (sitr_met && itr == eitr) {
					if (!sitr_is_begin)
						retit = iterator(i - 1, this);
					else
						retit = iterator(0, this);
					eitr_met = true;
				}
				++i;
			}
		}
		if (eitr_is_end) {
			if (sitr_is_begin)
				retit = iterator(0, this);
			else
				retit = iterator(i - 1, this);
		}
		if (!sitr_met || !eitr_met && !eitr_is_end)
			throw IllegalArgumentException{};
		theSize -= erase_count;
		std::swap(objects, new_objects);
		delete[] new_objects;
		return retit;
	}
	iterator insert(iterator pitr, Object const & x) {
		if (pitr == end()) {
			push_back(x);
			return end() - 1;
		}
		Object * new_objects = new Object[++theCapacity];
		iterator retit;
		unsigned int i = 0;
		bool inserted = false;
		for (iterator itr = begin(); itr != end(); ++itr) {
			if (itr == pitr) {
				retit = &new_objects[i];
				new_objects[i++] = x;
				inserted = true;
			}
			new_objects[i++] = *itr;
		}
		if (!inserted) {
			--theCapacity;
			throw IllegalArgumentException{};
		}
		++theSize;
		std::swap(objects, new_objects);
		delete[] new_objects;
		return retit;
	}
	iterator insert(iterator pitr, Object && x) {
		if (pitr == end()) {
			push_back(std::move(x));
			return end() - 1;
		}
		Object * new_objects = new Object[++theCapacity];
		iterator retit;
		unsigned int i = 0;
		bool inserted = false;
		for (iterator itr = begin(); itr != end(); ++itr) {
			if (itr == pitr) {
				retit = iterator(i, this);
				new_objects[i++] = std::move(x);
				inserted = true;
			}
			new_objects[i++] = *itr;
		}
		if (!inserted) {
			--theCapacity;
			throw IllegalArgumentException{};
		}
		++theSize;
		std::swap(objects, new_objects);
		delete[] new_objects;
		return retit;
	}
	bool operator==(MyVector<Object> const & other) const
	{
		if (size() != other.size())
			return false;
		for (unsigned int i = 0; i != size(); ++i) {
			if ((*this)[i] != other[i])
				return false;
		}
		return true;
	}
	iterator begin()
	{
		//return iterator(0, this);
		MyVector<Object>::iterator temp = iterator{ 0, this };
		return temp;
	}
	const_iterator begin() const
	{
		return const_iterator(0, this);
	}
	iterator end()
	{
		return iterator(size(), this);
	}
	const_iterator end() const
	{
		MyVector<Object>::const_iterator temp = const_iterator(size(), this);
		return temp;
	}

	static const int SPARE_CAPACITY = 2;
};

#endif